var express = require('express'),
    app = express(),
    server = require('http').createServer(app);

server.listen(80);

app.use("/node_modules", express.static('node_modules'));
app.use(express.static('www'));

    app.get('/', function(req, res){
        res.sendFile(__dirname + '/www/index.html');
    });
