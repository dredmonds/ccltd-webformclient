/*****************************************************************
* Module Name: invoice
* Description: invoice
* Author: HRoy
* Date Modified: 2016.07.28
* Related files: invoice.html
*****************************************************************/
app.controller('invoiceCtrl', function($scope, $state, $http, $stateParams, $ionicPopup, $window) {

	$scope.invoice = function() {
	};

	$scope.load = function(){
		var todaysDate	= new Date();
		var today				= todaysDate.getFullYear() + ", " + (todaysDate.getMonth()+1) + ", " + todaysDate.getDate();

		$scope.buyerName						= "John Lewis PLC";
		$scope.supplierName					= "New Ventures LTD.";
		$scope.invoiceNumber				= "(unassigned)";

		$scope.buyerEAN							= "5023949000004";
		$scope.invoiceDate					= new Date(today);

		$scope.buyerVAT							= "000000000";
		$scope.supplierEAN					= "5099999999999";

		$scope.Code									= "123456";
		$scope.taxDate							= new Date(today);

		$scope.deliveryLocName			= "JOHN LEWIS HIGH WYCOMBE";
		$scope.supplierVAT					= "111111111";

		$scope.deliveryLocEAN				= "5023949572529";
		$scope.paymentTerms					= "Pay by specified date";

		$scope.deliveryLocCode			= "HIGHWYCOMBE";
		$scope.orderNumber					= "460880/820";
		$scope.paymentDate					= new Date(today);

		$scope.supplierLocCode			= "xxyyy";
		$scope.orderDate						= "08/07/2009";
		$scope.daysToPayment				= "30";

		$scope.deliveryAddress1			= "UNIT M";
		$scope.deliveryNoteNumber		= "";
		$scope.settlementPercent		= "0";

		$scope.deliveryAddress2			= "TURNPIKE WAY, CASTLE ESTATE";
		$scope.deliveryNoteDate			=	new Date(today);
		$scope.surchargesExVATa			= "0.00"
		$scope.surchargesExVATb			= "Carriage"
		$scope.surchargesExVATc			= "Z-0%"

		$scope.deliveryAddress3			= "CRESSEX";
		$scope.goodsReceiptNumber		= "x34";
		$scope.currency							= "GBP";

		$scope.deliveryAddress4			= "HIGH WYCOMBE";
		$scope.goodsReceiptDate			= new Date(today);

		$scope.deliveryPostCode			= "HP12 3TF";



	};

	$scope.goOrder = function(){
		$state.go('order');
	}

})
