/*****************************************************************
* Module Name: Web Form Main Menu
* Description: To display content of main webform
* Author: dredmonds
* Date Modified: 2016.09.23
* Related files: wFormMain.html
*****************************************************************/

app.controller('wFormMainCtrl', function($scope, $rootScope, $state, $ionicHistory, $ionicPopup, $http) {

  var pOrderData = {};
  
  $scope.createPO = function(){
    $scope.pOrderData = {};
  };
  
  $scope.editPO = function(){};
  
  $scope.savePO = function(pOrderData){
	//var itemList = [];
	/*function loadItemList(value){
	  itemList.push(value);	
	}*/
	
	//itemList = pOrderData.lineItems
	//pOrderData.lineItems.forEach(loadItemList);
	
	
    var poObj = {
        orderNumber    : pOrderData.orderNumber,
        orderDate      : pOrderData.orderDate,
        customerName   : pOrderData.customerName,
   
        supplierName   : pOrderData.supplierName,
        supplierEAN    : pOrderData.supplierEAN,
        supplierCode   : pOrderData.supplierCode,
     
        addRemarks     : pOrderData.addRemarks,
        
        lineItems      : pOrderData.lineItems,
        orderStatus    : pOrderData.orderStatus,
        formStatus     : pOrderData.formStatus,
        ediReference   : pOrderData.ediReference,
        txnReference   : pOrderData.txnReference,
        fileGenNumber  : pOrderData.fileGenNumber,
    };
    
	$scope.baseURL = window.localStorage.getItem('baseURL');
    $http.post($scope.baseURL + '/orders', poObj, {headers: {'Content-Type': 'application/json'}})
    .then(function(data, status, headers, config){

      //success
      var result = data.data;
      $ionicPopup.alert({
        title: "Save Order",
        template: result.result
      });

    },function(data, status, headers, config){

      //Error
      $ionicPopup.alert({
        title: "Error:",
        template: data.data.result
      })
    });   
    
    
    
  
  };


  $scope.load = function(){
     $scope.pOrderData = {
       msgID          : 'ORD12345678',
       orderNumber    : '12345678',
       orderDate      : '2016-10-06',
       customerName   : 'The Retailer Company',
  
       supplierName   : 'The Supplier Company',
       supplierEAN    : '12345678',
       supplierCode   : '98765432',
    
       addRemarks     : 'Additional messages and instructions.',
       
       lineItems      : [{line:'1', 
                           supplierTUEAN: '5000000003781',
                           supplierItemCode: 'EDAM CHEESE SM',
                           buyerItemCode: '63758750',
                           CUinTU: '1',
                           quantity: '1 x 333.5M',
                           itemPrice: '19.36',
                           description: 'FINE WORCESTER TW FW48 400 SM'},
                         {line:'2', 
                           supplierTUEAN: '5000000003782',
                           supplierItemCode: 'EDAM CHEESE MD',
                           buyerItemCode: '63758751',
                           CUinTU: '1',
                           quantity: '15',
                           itemPrice: '15',
                           description: 'FINE WORCESTER TW FW48 400 MD'},
                         ],
        orderStatus    : 'In Progress',
        formStatus     : 'Active',
        ediReference   : '123456',
        txnReference   : '0430',
        fileGenNumber  : '5565',
    };/*End of pOrderData*/
    
    /*set order date to dateformat*/	
	$scope.dt = new Date($scope.pOrderData.orderDate);
  }/*End of load function*/	
	
	
});