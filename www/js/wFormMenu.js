/*****************************************************************
* Module Name: Web Form Main Menu
* Description: To display menu, header and navigation link
* Author: dredmonds
* Date Modified: 2016.09.23
* Related files: wFormMenu.html
*****************************************************************/

app.controller('wFormMenuCtrl', function($scope, $rootScope, $state, $ionicHistory, $mdEditDialog, $q, $timeout, $mdSidenav, $mdToast) {

	
$scope.load = function(){	
  /******* Menu Data *******/
  $scope.wFormMenuData = {
      title: 'CC WebForm - Management Solution',
      user: {
        name: 'Ed Reyes',
        email: 'ereyes@cc-ltd.com',
        icon: 'face'
      },
      toolbar: {
        buttons: [{
          name: 'Button 1',
          icon: 'add',
          link: 'Button 1'
        }],
        menus: [{
          name: 'Menu 1',
          icon: 'message',
          width: '4',
          actions: [{
            name: 'Action 1',
            message: 'Action 1',
            completed: true,
            error: true
          }, {
            name: 'Action 2',
            message: 'Action 2',
            completed: false,
            error: false
          }, {
            name: 'Action 3',
            message: 'Action 3',
            completed: true,
            error: true
          }]
        }]
      },
     sidenav: {
       menus:[{
         name: 'Profile Settings',
         icon: 'settings',
         width: '4',
             actions: [{
               name: 'Edit profile',
               message: 'click-editProfile',
               completed: true,
               error: true
             }, {
               name: 'Other Actions',
               message: 'click-otherActions',
               completed: false,
               error: false
             }, {
               name: 'Sign out',
               message: 'click-signOut',
               completed: true,
               error: true
             }]
       }],
       sections: [{
         name: 'Inbox',
         expand: true,
         actions: [{
           name: 'Orders',
           icon: 'mail_outline',
           link: 'click-orders'
           }, {
           name: 'Sales/Stock Report',
           icon: 'mail_outline',
           link: 'click-reportsSalesStock'
           }]
         }, {
         name: 'Drafts',
         expand: false,
         actions: [{
           name: 'Draft Messages',
           icon: 'drafts',
           link: 'click-draftMessages'
           }]
         }, {
         name: 'Sent',
         expand: false,
         actions: [{
           name: 'Delivery Note(ASN)',
           icon: 'send',
           link: 'click-deliveryNotesASN'
           }, {
  		    name: 'Invoices',
           icon: 'send',
           link: 'click-invoice'			  
           }]
         }, {
         name: 'Trading Partners',
         expand: false,
         actions: [{
           name: 'Company 1',
           icon: 'settings',
           link: 'click-companyName1'
           }, {
           name: 'Company 2',
           icon: 'settings',
           link: 'click-companyName2'
           }, {
           name: 'Company 3',
           icon: 'settings',
           link: 'click-companyName3'
           }]
         }, {
         name: 'Archives',
         expand: false,
         actions: [{
           name: 'Documents Archive',
           icon: 'archive',
           link: 'click-Archive'
           }]
  	    }]/*End of sections*/
     }/*End of sidenav*/
  } /******* End of Menu Data *******/
  
}/*End of load function*/ 
	
  $scope.toggleSidenav = function(menu) {
    $mdSidenav(menu).toggle();
  }
  
  $scope.toast = function(message) {
    var toast = $mdToast.simple().content('You clicked ' + message).position('bottom right');
    $mdToast.show(toast);
  
    if(message === 'click-orders'){
    	$state.go('webForm.main');
    }
  };
  
  $scope.toastList = function(message) {
    var toast = $mdToast.simple().content('You clicked ' + message + ' having selected ' + $scope.selected.length + ' item(s)').position('bottom right');
    $mdToast.show(toast);
  
  };
  
  
  
	
});