/*****************************************************************
* Module Name: order
* Description: order forms (Prototype version)
* Author: dredmonds
* Date Modified: 2016.08.01
* Related files: order.html
*****************************************************************/

app.controller('orderCtrl', function($scope, $rootScope, $state, $ionicHistory, $mdEditDialog, $q, $timeout, $mdSidenav, $mdToast) {

   $scope.selected = [];
   $scope.limitOptions = [5, 10, 15];
   $scope.options = {
     rowSelection: true,
     multiSelect: true,
     autoSelect: true,
     decapitate: false,
     largeEditDialog: false,
     boundaryLinks: false,
     limitSelect: true,
     pageSelect: true
   };	
   $scope.query = {
     order: 'name',
     limit: 5,
     page: 1
   };

   $scope.toggleLimitOptions = function () {
     $scope.limitOptions = $scope.limitOptions ? undefined : [5, 10, 15];
   };
   
   $scope.getTypes = function () {
     return ['Invoice', 'Order', 'Stock Report'];
   };
   
   $scope.loadStuff = function () {
     $scope.promise = $timeout(function () {
       // loading
     }, 2000);
   }
   
   $scope.logItem = function (item) {
     console.log(item.name, 'was selected');
     $state.go('webForm.order.details');
   };
   
   $scope.logOrder = function (order) {
     console.log('order: ', order);
   };
   
   $scope.logPagination = function (page, limit) {
     console.log('page: ', page);
     console.log('limit: ', limit);
   }
   
   $scope.viewDetails = function(selected){
	 console.log(selected[0].docNumber.value, 'was selected on the list');
     $state.go('webForm.order.details');  
   };

 
 $scope.load = function() { 
 
   /*WebForm Data*/
   $scope.webFormMsg = {
       "count": 9,
       "data": [
         {
           "name": "John Lewis PLC",
           "type": "Order",
           "docNumber":   { "value": "520314/546" },
           "docStatus":   { "value": "In Progress" },
           "docDate":     { "value": "22/07/2016" },
           "docAction":   { "value": "Invoice Created" },
           "dateCreated": { "value": "21/07/2016" },
           "dateModify":  { "value": "23/07/2016" },
           "fileGenNum":  { "value": "101" }
         }, {
           "name": "Argos PLC",
           "type": "Sales/Stock Report",
           "docNumber":   { "value": "524148/546" },
           "docStatus":   { "value": "In Progress" },
           "docDate":     { "value": "14/02/2016" },
           "docAction":   { "value": "No Invoice" },
           "dateCreated": { "value": "15/02/2016" },
           "dateModify":  { "value": "16/02/2016" },
           "fileGenNum":  { "value": "201" }
         }, {
           "name": "Sainsbury PLC",
           "type": "Order",
           "docNumber":   { "value": "174806/637" },
           "docStatus":   { "value": "New" },
           "docDate":     { "value": "12/04/2016" },
           "docAction":   { "value": "No Invoice" },
           "dateCreated": { "value": "11/04/2016" },
           "dateModify":  { "value": "13/04/2016" },
           "fileGenNum":  { "value": "301" }
         }, {
           "name": "Boots UK PLC",
           "type": "Sales/Stock Report",
           "docNumber":   { "value": "174933/637" },
           "docStatus":   { "value": "In Progress" },
           "docDate":     { "value": "14/03/2016" },
           "docAction":   { "value": "No Invoice" },
           "dateCreated": { "value": "16/03/2016" },
           "dateModify":  { "value": "15/03/2016" },
           "fileGenNum":  { "value": "401" }
         }, {
           "name": "TESCO PLC",
           "type": "Order",
           "docNumber":   { "value": "174968/637" },
           "docStatus":   { "value": "In Progress" },
           "docDate":     { "value": "25/05/2016" },
           "docAction":   { "value": "No Invoice" },
           "dateCreated": { "value": "26/05/2016" },
           "dateModify":  { "value": "27/05/2016" },
           "fileGenNum":  { "value": "501" }
         }, {
           "name": "Mark and Spencer PLC",
           "type": "Sales/Stock Report",
           "docNumber":   { "value": "460880/820" },
           "docStatus":   { "value": "New" },
           "docDate":     { "value": "17/07/2016" },
           "docAction":   { "value": "No Invoice" },
           "dateCreated": { "value": "18/07/2016" },
           "dateModify":  { "value": "19/07/2016" },
           "fileGenNum":  { "value": "601" }
         }, {
           "name": "Debenhams PLC",
           "type": "Order",
           "docNumber":   { "value": "460888/830" },
           "docStatus":   { "value": "In Progress" },
           "docDate":     { "value": "07/07/2016" },
           "docAction":   { "value": "Invoice Created" },
           "dateCreated": { "value": "08/07/2016" },
           "dateModify":  { "value": "09/07/2016" },
           "fileGenNum":  { "value": "701" }
         }, {
           "name": "Fat Face PLC",
           "type": "Sales/Stock Report",
           "docNumber":   { "value": "147833/692" },
           "docStatus":   { "value": "In Progress" },
           "docDate":     { "value": "13/07/2016" },
           "docAction":   { "value": "No Invoice" },
           "dateCreated": { "value": "15/07/2016" },
           "dateModify":  { "value": "14/07/2016" },
           "fileGenNum":  { "value": "801" }
         }, {
           "name": "Square Box PLC",
           "type": "Order",
           "docNumber":   { "value": "560430/750" },
           "docStatus":   { "value": "New" },
           "docDate":     { "value": "21/06/2016" },
           "docAction":   { "value": "No Invoice" },
           "dateCreated": { "value": "20/06/2016" },
           "dateModify":  { "value": "22/06/2016" },
           "fileGenNum":  { "value": "901" }
         }
       ]
   };/*End of WebForm Data*/
   
 };/*End of Load Function*/	
   
});
