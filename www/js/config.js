app.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
  $urlRouterProvider.otherwise('/');
  $ionicConfigProvider.views.maxCache(0);
  $stateProvider
  .state('index',{
    url: '/',
    templateUrl: 'index.html',
    controller: 'indexCtrl'
  })

  .state('webForm',{
	url:'/webForm',
    templateUrl: 'view/wFormMenu.html',
    controller: 'wFormMenuCtrl'
  })
  
  .state('webForm.main',{
    url: '/main',
    views: {
      'content':{
        templateUrl: 'view/wFormMain.html',
    	controller: 'wFormMainCtrl'	  
      }
    }
  })
  
  .state('webForm.order',{
    url: '/order',
    views: {
      'content':{
        templateUrl: 'view/order.html',
    	controller: 'orderCtrl'
      }
    }
  })  
  
  .state('webForm.order.details',{
    url: '/details',
    views: {
      'content':{
        templateUrl: 'view/orderDetails.html',
    	controller: 'orderDetailsCtrl'
      }
    }
  })  
  
  .state('webForm.invoice',{
    url: '/invoice',
    views: {
      'content':{
        templateUrl: 'view/invoice.html',
    	controller: 'invoiceCtrl'
      }
    }
  })


})
