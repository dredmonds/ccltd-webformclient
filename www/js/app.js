// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('webform', ['ionic', 'ngMaterial', 'ui.grid', 'ui.bootstrap', 'md.data.table'])

  
  //Store the server address into Constant.
  .constant('baseURL', 'http://webform.cc-ltd.com:3000')
  .constant('localURL', 'http://localhost:3000')
  .constant('loginURL', 'https://webform.cc-ltd.com:3333')
  .constant('localLoginURL', 'https://localhost:3333')


  .run(function($state, $rootScope, $ionicLoading, $ionicPlatform, $ionicPopup, $http, $window, $location, baseURL, localURL, loginURL, localLoginURL) {
	  
    console.log("App start");
    //Store the server address into localStorage, this make one place to modify the server address in the future.
    var localhost = true;
    if(localhost){
      window.localStorage.setItem("baseURL", localURL);
      window.localStorage.setItem("loginURL", localLoginURL);
    }else{
      window.localStorage.setItem("baseURL", baseURL);
      window.localStorage.setItem("loginURL", loginURL);
    }
    
    var httpURL = window.localStorage.getItem("baseURL");
    var httpsURL = window.localStorage.getItem("loginURL");
    $rootScope.baseURL = httpURL;
    $rootScope.loginURL = httpsURL;	  
	  
	  
	  
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
  
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
  
    });
  })
  
  .config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
  });