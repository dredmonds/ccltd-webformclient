/*****************************************************************
* Module Name: order details
* Description: order details information page
* Author: dredmonds
* Date Modified: 2016.09.26
* Related files: orderDetails.html
*****************************************************************/

app.controller('orderDetailsCtrl', function($scope, $rootScope, $state, $ionicHistory, $ionicPopup) {
  
  $scope.load = function(){
    $scope.pOrderData = {
      orderNumber    : '12345678',
      orderDate      : '20160822',
      customerName   : 'The Retailer Company',

      supplierName   : 'The Supplier Company',
      supplierEAN    : '12345678',
      supplierCode   : '98765432',
	  
	  addRemarks     : 'Additional messages and instructions.',
      
      lineItems      : [{line:'1', 
                          supplierTUEAN: '5000000003781',
                          supplierItemCode: 'EDAM CHEESE SM',
                          buyerItemCode: '63758750',
                          CUinTU: '1',
                          quantity: '1 x 333.5M',
                          itemPrice: '19.36',
                          description: 'FINE WORCESTER TW FW48 400 SM'},
                        {line:'2', 
                          supplierTUEAN: '5000000003782',
                          supplierItemCode: 'EDAM CHEESE MD',
                          buyerItemCode: '63758751',
                          CUinTU: '1',
                          quantity: '15',
                          itemPrice: '15',
                          description: 'FINE WORCESTER TW FW48 400 MD'},
                        ],
       orderStatus    : 'In Progress',
       formStatus     : 'Active',
       ediReference   : '123456',
       txnReference   : '0430',
       fileGenNumber  : '5565',
	};/*End of pOrderData*/
    
  }/*End of load function*/
   
});
